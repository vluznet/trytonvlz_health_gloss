#This file is part of Tryton.  The COPYRIGHT file at the top level
#of this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from trytond.pool import PoolMeta, Pool
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.model import fields

__all__ = ['Invoice', 'GlossDetailedReport']



class Invoice:
    __metaclass__ = PoolMeta
    __name__ = 'account.invoice'
    glosses = fields.One2Many('gnuhealth.gloss', 'out_invoice', 'Glosses')

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()


class GlossDetailedReport(Report):
    __name__ = 'gnuhealth.gloss_detailed'

    @classmethod
    def parse(cls, report, objects, data, localcontext):
        user = Pool().get('res.user')(Transaction().user)
        localcontext['company'] = user.company
        return super(GlossDetailedReport, cls).parse(report,
                objects, data, localcontext)
